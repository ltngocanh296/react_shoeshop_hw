import React, { Component } from 'react'
import ItemShoe from './ItemShoe'

export default class ListShoe extends Component {
    renderShoeList= ()=>{
        return this.props.shoeArr.map((item) => { 
            return <ItemShoe 
            handleViewDetails={this.props.handleChangeDetails}
            handleAddToCart={this.props.handleAddCart}
            data={item}/>
         });
    }
  render() {
    return (
      <div className='row'>{this.renderShoeList()}</div>
    )
  }
}

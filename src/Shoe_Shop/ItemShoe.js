import React, { Component } from 'react'

export default class ItemShoe extends Component {
  render() {
    let {name,image,price}=this.props.data
    return (
      <div className='col-3 p-1'>
        <div className="card text-left ">
          <img className="card-img-top" src={image} alt />
          <div className="card-body">
            <h4 className="card-title">{name}</h4>
            <h4 className="card-title text-danger">{price}$</h4>
            <button onClick={() => {this.props.handleAddToCart(this.props.data) }}className='btn btn-success mr-3'>Buy</button>
            <button onclick = {() => { this.props.handleViewDetails(this.props.data) }}className='btn btn-primary'>Details</button>
          </div>
        </div>
      </div>
    );
  }
}

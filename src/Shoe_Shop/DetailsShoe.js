import React, { Component } from 'react'

export default class DetailsShoe extends Component {
  render() {
    return (
      <div className='row'>
        <img src={this.props.details.image} alt="" className='col-4'></img>
        <div className="col-8">
            <p>{this.props.details.name}</p>
            <p>{this.props.details.price}</p>
            <p>{this.props.details.description}</p>
        </div>
        
      </div>
    )
  }
}

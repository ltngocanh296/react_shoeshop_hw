import React, { Component } from 'react'
import CartShoe from './CartShoe'
import { dataShoe } from './dataShoe'
import DetailsShoe from './DetailsShoe'
import ListShoe from './ListShoe'
export default class ShoeShop extends Component {
    state ={
        shoeArr:dataShoe,
        details:dataShoe[0],
        addCart:[],
    }
    handleChangeDetail=(value)=>{
      this.setState({details:value});
      
    }
    handleAddCart =(shoe)=>{
      let cloneCart = [...this.state.addCart]

      let index = this.state.addCart.findIndex((item) => { 
        return item.id == shoe.id;
       });
       if (index == -1){
        let cartItem={...shoe,number:1}
        cloneCart.push(cartItem);
       }else{
        cloneCart[index].number ++;
       }
       this.setState({
        addCart:cloneCart,
       })
    }
  render() {
    return (
      <div>
        <CartShoe cart={this.state.addCart}/>
        <ListShoe shoeArr={this.state.shoeArr} 
        handleChangeDetail={this.handleChangeDetail}
        handleAddCart={this.handleAddCart}
        /> 
        <DetailsShoe details={this.state.details}/>


      </div>
    )
  }
}

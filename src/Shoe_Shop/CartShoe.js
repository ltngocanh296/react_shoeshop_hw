import React, { Component } from 'react'

export default class CartShoe extends Component {
  renderListAdd=()=>{
    return this.props.cart.map((item) => { 
        return (
            <tr>
                <td>{item.id}</td>
                <td>{item.name}</td>
                <td>{item.price *item.number}</td>
                <td>{item.shortDescription}</td>
                <td>{item.number}</td>

                <td><img style={{width:"80px"}} src={item.image} alt="" /> </td>
            </tr>
        );
     });
    };
  render() {
    return (
      <table className='table'>
        <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Price</th>
                <th>Short Desc</th>
                <th>SL</th>
                <th>Image</th>
                
            </tr>
        </thead>
        <tbody>{this.renderListAdd()}</tbody>
      </table>
    );
  }
}
